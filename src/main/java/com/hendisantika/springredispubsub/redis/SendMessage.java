package com.hendisantika.springredispubsub.redis;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-redis-pubsub
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/07/18
 * Time: 19.07
 * To change this template use File | Settings | File Templates.
 */
@Data
public class SendMessage {

    private String name;

    private String email;

}